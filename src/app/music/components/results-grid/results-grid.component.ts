import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Album } from 'src/app/core/model/album';
import { Playlist } from 'src/app/playlists/model/Playlist';

@Component({
  selector: 'app-results-grid',
  templateUrl: './results-grid.component.html',
  styleUrls: ['./results-grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsGridComponent implements OnInit {

  @Input() results: Album[] = []

  constructor() { }

  ngOnInit(): void {
  }

}
