import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError, concatAll, exhaust, exhaustMap, filter, map, mergeAll, switchAll, switchMap, tap } from 'rxjs/operators';
import { Album } from 'src/app/core/model/album';
import { AlbumSearchService } from 'src/app/core/services/album-search/album-search.service';

@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.component.html',
  styleUrls: ['./album-search.component.scss']
})
export class AlbumSearchComponent implements OnInit {
  message = '';

  SEARCH_PARAM = 'q'

  query = this.route.queryParamMap.pipe(
    map(params => params.get(this.SEARCH_PARAM)),
    filter((q): q is string => q !== null),
  )

  results = this.query.pipe(switchMap(query => this.service.getAlbums(query)),
    catchError((error) => {
      this.message = error.message
      return []
    })
  )

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: AlbumSearchService
  ) { }

  ngOnInit(): void { }

  search(query: string) {
    this.router.navigate(['.'], {
      queryParams: { [this.SEARCH_PARAM]: query },
      relativeTo: this.route
    })
  }

}
