import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, filter, map, switchMap } from 'rxjs/operators';
import { AlbumSearchService } from 'src/app/core/services/album-search/album-search.service';


/* 
  TODO:
  - load this on /music/albums?id=5Tby0U5VndHW0SomYO7Id7
  - show Id in html
  - fetch Alubm from server
  - show album Image, name
  - search results -> click card -> redirects here
*/

@Component({
  selector: 'app-album-details-view',
  templateUrl: './album-details-view.component.html',
  styleUrls: ['./album-details-view.component.scss']
})
export class AlbumDetailsViewComponent implements OnInit {

  

  ngOnInit(): void {
  }


  
  message = '';

  SEARCH_PARAM = 'id'

  query = this.route.queryParamMap.pipe(
    map(params => params.get(this.SEARCH_PARAM)),
    filter((id): id is string => id !== null),
  )

  results = this.query.pipe(switchMap(query => this.service.getAlbumById(query)),
    catchError((error) => {
      this.message = error.message
      return []
    })
  )
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: AlbumSearchService
  ) { }



  search(query: string) {
    this.router.navigate(['.'], {
      queryParams: { [this.SEARCH_PARAM]: query },
      relativeTo: this.route
    })
  }

}


