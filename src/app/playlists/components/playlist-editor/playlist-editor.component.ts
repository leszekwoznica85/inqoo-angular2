import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Playlist } from '../../model/Playlist';
@Component({
  selector: 'app-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.scss']
})
export class PlaylistEditorComponent implements OnInit/* , OnChanges */ {
  @Input() playlist: Playlist = {
    id: '',
    name: '',
    public: false,
    description: '',
  }
  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter<Playlist>();

  clickCancel() { this.cancel.emit() }
  

  /**
   * Get access to form directive from COnsole
   * ng.getDirectives(document.querySelector('form')) 
   * ... [...ngForm...]
   */
  

  extra = false

  submit(formRef: NgForm) {
    if (formRef.invalid) {
      return
    }

    this.save.emit({
      ...this.playlist,
      // name: formRef.value['name']
      ...formRef.value
    })
  }


  constructor() {
    // this.draft = this.playlist // Inputs are not there yet!
  }

  ngOnInit(): void {
    // only once
    // this.draft = { ...this.playlist }
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.

  }
}
