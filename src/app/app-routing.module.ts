import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { PlaylistsViewComponent } from './playlists/containers/playlists-view/playlists-view.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/playlists',
    pathMatch: 'full' // prefix
  },
  {
    path: 'music',
    loadChildren: () =>
      import('./music/music.module')
        .then(m => m.MusicModule)
  },
  {
    path: '**', // wildcard - match all,
    // component: PageNotFoundComponent
    redirectTo: 'music'
  }
  // ....PlaylistsRoutingModule
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
